import { scrollToSmoothly } from '../../utils.js'
import Books from './books.js'

export const name = 'epub阅读器'

export const render = async ({ root }) => {
  root.style.overflowY = 'auto'
  root.style.display = 'flex'
  root.style.flexDirection = 'column'
  root.style.position = 'relative'
  root.tabIndex = -1
  root.innerHTML = `
    <div id="book-content" class="flex-1 overflow-y-auto" style="padding: 8px;color: #888;"></div>
    <div id="footer" class="flex hide" style="line-height: 30px;border-top: solid 1px #ccc;transition: opacity 0.3s ease-in-out;opacity: 0.5">
      <div class="flex-1 text-center pointer" style="border-right: solid 1px #ccc;" id="prev">上一章</div>
      <div class="flex-1 text-center pointer" style="border-right: solid 1px #ccc;" id="nav">目录</div>
      <div class="flex-1 text-center pointer" style="flex: 1;" id="next">下一章</div>
    </div>
    <div id="book-nav" class="fullscreen absolute hide top-0 left-0 bg-white"></div>
    <div id="book-list" class="absolute hide fullscreen top-0 left-0 bg-white" style="color: #888;padding: 10px">
      <input
        id="upload-input"
        placeholder="输入epub链接"
        style="width: 90%;border: solid 1px #ccc;border-radius: 4px;padding: 4px 8px;margin-bottom: 10px;"
      />
      <div id="books"></div>
    </div>
  `

  const bookContent = root.querySelector('#book-content')
  const bookNav = root.querySelector('#book-nav')
  const prevBtn = root.querySelector('#prev')
  const nextBtn = root.querySelector('#next')
  const navBtn = root.querySelector('#nav')
  const footer = root.querySelector('#footer')
  const bookList = root.querySelector('#book-list')
  const booksContainer = root.querySelector('#books')
  const uploadInput = root.querySelector('#upload-input')

  const bookManager = new Books({
    container: bookContent,
    navContainer: bookNav,
  })

  const state = {
    isFocus: false,
  }
  root.addEventListener('focus', () => {
    state.isFocus = true
    footer.style.opacity = 1
  })
  root.addEventListener('blur', () => {
    state.isFocus = false
    footer.style.opacity = 0.5
  })

  const renderBookList = () => {
    const { books } = bookManager
    booksContainer.innerHTML = books.map((p, idx) => `
      <a
        href="javascript:void(0)"
        class="block"
        style="width: 100%"
        data-id="${p.uuid}"
        title="${p.url}"
      >${idx+1}. ${p.bookName}</a>
    `).join('')
  }

  uploadInput.addEventListener('keydown', async (ev) => {
    if (ev.key === 'Enter') {
      ev.preventDefault()
      ev.stopPropagation()
      const url = uploadInput.value
      if (!url) return
      bookList.classList.add('hide')
      footer.classList.remove('hide')
      uploadInput.value = ''

      bookManager.addAndRenderBook(url)
    }
  })

  window.$eventbus.on('appKeydown', (ev) => {
    if (!state.isFocus) return
    const key = ev.data
    if (key === 'ArrowRight') {
      bookManager.currentBookNextPage()
    } else if (key === 'ArrowLeft') {
      bookManager.currentBookPrevPage()
    } else if (key === 'ArrowDown') {
      scrollToSmoothly(bookContent, 200)
    } else if (key === 'ArrowUp') {
      scrollToSmoothly(bookContent, -200)
    }
  })

  prevBtn.addEventListener('click', async (ev) => {
    bookManager.currentBookPrevPage()
  })

  nextBtn.addEventListener('click', async (ev) => {
    bookManager.currentBookNextPage()
  })

  navBtn.addEventListener('click', async (ev) => {
    bookNav.classList.remove('hide')
  })

  bookNav.addEventListener('click', async (ev) => {
    const { url } = ev.target.dataset
    if (!url) return
    if (url === 'exit') {
      bookManager.exitCurrentBook()
      bookContent.innerHTML = ''
      renderBookList()
      bookList.classList.remove('hide')
      footer.classList.add('hide')
      bookNav.classList.add('hide')
      return
    }
    console.warn('bookNav', url)
    bookManager.currentBookGoPageByUrl(url)
    bookNav.classList.add('hide')
  })

  booksContainer.addEventListener('click', (ev) => {
    const { id } = ev.target.dataset
    if (!id) return
    ev.preventDefault();
    bookManager.openBook(Number(id))
    bookList.classList.add('hide')
    footer.classList.remove('hide')
  })

  await bookManager.initBooks()
  const currentBook = bookManager.currentBook()
  if (currentBook) {
    bookList.classList.add('hide')
    footer.classList.remove('hide')
  } else {
    renderBookList()
    bookList.classList.remove('hide')
    footer.classList.add('hide')
  }
}

export const unmount = ({ root }) => {
  root.innerHTML = ''
}
