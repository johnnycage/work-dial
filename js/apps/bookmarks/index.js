import { setStorage, getStorage } from '../../bridge.js'

export const name = '快捷书签'

const { Button, Modal, Input, Form, Tag } = window.antd
const h = window.React.createElement

class Bookmarks extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showDialog: false,
      bookmarks: [],
    }
    this.form = React.createRef()

    this.loadBookmarks()
  }

  async loadBookmarks() {
    this.setState({
      bookmarks: (await getStorage('bookmarks')) || [],
    })
  }

  add = () => {
    this.setState({
      showDialog: true,
    })
  }

  del = (item) => {
    return () => {
      const { bookmarks } = this.state
      const newBookmarks = bookmarks.filter((p) => p !== item)
      this.setState({
        bookmarks: newBookmarks,
      })
      setStorage('bookmarks', newBookmarks)
    }
  }

  onFinish = (values) => {
    const { bookmarks } = this.state
    const newBookmarks = [...bookmarks, values]
    this.setState({
      bookmarks: newBookmarks,
    })
    setStorage('bookmarks', newBookmarks)
  }

  render() {
    const { showDialog, bookmarks } = this.state
    return h(
      'div',
      {
        style: {
          display: 'flex',
          height: '100%',
          alignItems: 'center',
          padding: '0 12px',
        },
      },
      bookmarks.map((p) =>
        h(
          Tag,
          {
            key: p.href,
            closable: true,
            onClose: this.del(p),
            style: {
              borderRadius: 2,
            },
          },
          h(
            'a',
            {
              style: {
                display: 'inline-block',
                lineHeight: '28px',
              },
              href: p.href,
              target: '_blank',
            },
            p.name
          )
        )
      ),
      h(Button, { onClick: this.add }, '添加书签'),
      h(
        Modal,
        {
          title: '添加书签',
          open: showDialog,
          width: 400,
          okText: '保存',
          cancelText: '取消',
          onCancel: () => this.setState({ showDialog: false }),
          afterClose: () => this.form.current.resetFields(),
          onOk: async () => {
            const valid = await this.form.current.validateFields()
            if (valid) {
              this.form.current.submit()
              this.setState({
                showDialog: false,
              })
            }
          },
        },
        h(
          Form,
          {
            ref: this.form,
            name: 'basic',
            initialValues: {
              href: 'https://',
            },
            onFinish: this.onFinish,
            autoComplete: 'off',
          },
          h(
            Form.Item,
            {
              name: 'href',
              rules: [{ required: true, message: '请输入标签链接' }],
            },
            h(Input, { placeholder: '输入链接', allowClear: true })
          ),
          h(
            Form.Item,
            {
              name: 'name',
              rules: [{ required: true, message: '请输入标签名称' }],
            },
            h(Input, { placeholder: '输入名称', allowClear: true })
          )
        )
      )
    )
  }
}

let app;
export const render = ({ root }) => {
  app = ReactDOM.createRoot(root)
  app.render(h(Bookmarks, null))
}

export const unmount = () => {
  app && app.unmount()
}
