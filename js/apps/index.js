import * as RssReader from './rss-reader/index.js'
import * as Markdown from './markdown/index.js'
import * as Bookmarks from './bookmarks/index.js'
import * as Sudoku from './sudoku/index.js'
import * as BookReader from './book-reader/index.js'

export const normalApp = {
  RssReader,
  Markdown,
  Bookmarks,
  Sudoku,
  BookReader,
}
