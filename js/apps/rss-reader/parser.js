const domParser = new DOMParser();

export function parseXmlFeed(xml) {
  const trimmedInput = xml.trim();
  const dom = domParser.parseFromString(trimmedInput, "application/xml");
  const parser = [rssParser, atomParser].find((parser) => parser.isMatch(dom));
  if (!parser) {
    throw new Error("No parser found");
  }

  const { selectChannel, resolveChannel, selectItems, resolveItem } = parser;

  const channelElement = selectChannel(dom);

  return {
    ...resolveChannel(channelElement),
    items: Array.from(selectItems(dom)).map(resolveItem).filter(isArticle),
  };
}

export const rssParser = {
  isMatch: (root) => ["rss", "rdf:RDF"].includes(root.children[0]?.tagName),
  selectChannel: (root) => root.getElementsByTagName("channel")[0],
  selectItems: (root) => root.getElementsByTagName("item"),
  resolveChannel: (channelElement) => {
    const channelChildren = Array.from(channelElement.children);
    const homeUrl = channelChildren.find((node) => node.tagName === "link")?.textContent || undefined;

    return {
      title: parseChildByTagName(channelElement, "title")?.text() || "",
      homeUrl,
      items: [],
    };
  },
  resolveItem: (item) => {
    const itemChildren = Array.from(item.children);
    const decodedTitle = parseChildByTagName(item, "title")?.text();
    const date = itemChildren.find((node) => ["pubDate", "dc:date"].includes(node.tagName))?.textContent?.trim() || "";

    return {
      url: itemChildren.find((node) => node.tagName === "link")?.textContent?.trim() || undefined,
      title: decodedTitle,
      timePublished: coerceError(() => new Date(date || "").getTime(), Date.now()),
    };
  },
};

export const atomParser = {
  isMatch: (root) => root.children[0]?.tagName === "feed",
  selectChannel: (root) => root.getElementsByTagName("feed")[0],
  selectItems: (root) => root.getElementsByTagName("entry"),
  resolveChannel: (channelElement) => {
    const channelChildren = Array.from(channelElement.children);
    const homeUrl =
      channelChildren
        .find((node) => node.tagName === "link" && node.getAttribute("rel") !== "self")
        ?.getAttribute("href") || undefined;

    return {
      title: channelChildren.find((node) => node.tagName === "title")?.textContent || "",
      homeUrl,
      items: [],
    };
  },
  resolveItem: (item) => {
    const itemChildren = Array.from(item.children);
    const decodedTitle = itemChildren.find((node) => node.tagName === "title")?.textContent;
    const publishedDate = itemChildren.find((node) => node.tagName === "published")?.textContent;
    const modifedDate = itemChildren.find((node) => node.tagName === "updated")?.textContent;

    return {
      url:
        itemChildren
          .find((node) => node.tagName === "link")
          ?.getAttribute("href")
          ?.trim() || undefined,
      title: decodedTitle || undefined,
      timePublished: coerceError(() => new Date(publishedDate || modifedDate || "").getTime(), Date.now()),
    };
  },
};

function isArticle(item) {
  return !!item.title && !!item.url;
}

function parseChildByTagName(node, tagName) {
  return safeCall(
    parseXmlNode,
    Array.from(node.children).find((node) => node.tagName === tagName)
  );
}

function safeCall(fn, maybeValue, fallbackValue) {
  if (maybeValue === null || maybeValue === undefined) {
    return fallbackValue;
  } else {
    return fn(maybeValue);
  }
}

function parseXmlNode(node) {
  const text = () => xmlNodeToText(node);
  const html = () => xmlNodeToHtml(node);

  return {
    text,
    html,
  };
}

function xmlNodeToHtml(node) {
  if (Array.from(node.childNodes).some((node) => node instanceof CDATASection)) {
    return node.textContent?.trim() || "";
  } else {
    return unescapeString(node.innerHTML.trim());
  }
}

function unescapeString(escapedString) {
  const textarea = document.createElement("textarea");
  textarea.innerHTML = escapedString;
  return textarea.value;
}

function xmlNodeToText(node) {
  const parser = new DOMParser();
  const dom = parser.parseFromString(node.textContent?.trim() || "", "text/html");
  return dom.body.textContent || "";
}

function coerceError(fn, coerceTo) {
  try {
    return fn();
  } catch {
    return coerceTo;
  }
}
