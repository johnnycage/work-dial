export const createUniqueId = () => {
  return `${Date.now().toString(36)}${Math.random().toString(36).substring(2)}`
}

export const debounce = (callback, wait) => {
  let timeoutId = null
  return (...args) => {
    window.clearTimeout(timeoutId)
    timeoutId = window.setTimeout(() => {
      callback(...args)
    }, wait)
  }
}

export function scrollToSmoothly(el, delta, time = 100) {
  const currentPos = el.scrollTop
  let start = null
  const pos = currentPos + delta
  window.requestAnimationFrame(function step(currentTime) {
    start = !start ? currentTime : start
    const progress = currentTime - start
    if (currentPos < pos) {
      el.scrollTop = ((pos - currentPos) * progress) / time + currentPos
    } else {
      el.scrollTop = currentPos - ((currentPos - pos) * progress) / time
    }
    if (progress < time) {
      window.requestAnimationFrame(step)
    } else {
      el.scrollTop = pos
    }
  })
}

export const distinct = (arr, key) => {
  const res = []
  for (const item of arr) {
    if (!res.find(p => p[key] === item[key])) {
      res.push(item)
    }
  }
  return res
}